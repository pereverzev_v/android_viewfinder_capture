package com.example.viewfinderapp;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.ImageFormat;
import android.graphics.Rect;
import android.graphics.YuvImage;
import android.media.ImageReader;
import android.media.MediaActionSound;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Time;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import android.app.Activity;
import android.graphics.Matrix;
import android.graphics.RectF;
import android.hardware.Camera;
import android.hardware.Camera.CameraInfo;
import android.hardware.Camera.Size;
import android.os.Bundle;
import android.util.Log;
import android.view.Display;
import android.view.Surface;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;

public class MainActivity extends AppCompatActivity {

    SurfaceView sv;
    SurfaceHolder holder;
    HolderCallback holderCallback;
    Camera camera;
    ImageView mImageView;
    Button mCaptureButton;

    static String TAG = "viewFinderLog";

    byte[] mLastFrame;

    final int CAMERA_ID = 0;
    final boolean FULL_SCREEN = true;

    MediaActionSound shutterSound;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_main);

        sv = (SurfaceView) findViewById(R.id.surfaceView);
        holder = sv.getHolder();

        holderCallback = new HolderCallback();
        holder.addCallback(holderCallback);

        mImageView = findViewById(R.id.capturedImageView);

        mCaptureButton = findViewById(R.id.captureButton);

        shutterSound = new MediaActionSound();

        mCaptureButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {

                    ByteArrayOutputStream out = new ByteArrayOutputStream();

                    Camera.Size previewSize = camera.getParameters().getPreviewSize();

                    YuvImage yuvImage = new YuvImage(mLastFrame, camera.getParameters().getPreviewFormat(), previewSize.width, previewSize.height, null);

                    yuvImage.compressToJpeg(new Rect(0, 0, previewSize.width, previewSize.height), 100, out);

                    byte[] imageBytes = out.toByteArray();

                    Bitmap bitmapOrigin = BitmapFactory.decodeByteArray(imageBytes, 0, imageBytes.length);

                    Bitmap bitmap = rotateBitmap(bitmapOrigin, 90);

                    mImageView.setImageBitmap(bitmap);

                    /*Calendar cal = Calendar.getInstance();
                    DateFormat sdf = SimpleDateFormat.getTimeInstance();

                    File pictures = Environment
                            .getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);

                    File outFile = new File(pictures, sdf.format(cal.getTime()) + ".jpg");

                    FileOutputStream outputStream = new FileOutputStream(outFile);

                    bitmap.compress(Bitmap.CompressFormat.JPEG, 100, outputStream);

                    outputStream.close();*/

                    shutterSound.play(MediaActionSound.SHUTTER_CLICK);

                /*} catch (FileNotFoundException e) {
                    Log.e(TAG, "Saving received message failed with", e);
                } catch (IOException e) {
                    Log.e(TAG, "Saving received message failed with", e);*/
                } catch (NullPointerException e) {
                    Log.e(TAG, "Saving received message failed with", e);
                }
            }
        });
    }

    public static Bitmap rotateBitmap(Bitmap source, float angle)
    {
        Matrix matrix = new Matrix();
        matrix.postRotate(angle);
        return Bitmap.createBitmap(source, 0, 0, source.getWidth(), source.getHeight(), matrix, true);
    }

    @Override
    protected void onResume() {
        super.onResume();
        camera = Camera.open(CAMERA_ID);

        setPreviewSize(FULL_SCREEN);

        Log.d(TAG, "onResume");
    }

    private void setCameraPreviewSize() {
        Camera.Parameters params = camera.getParameters();

        params.setPreviewSize(3150, 1171);

        camera.setParameters(params);
    }

    @Override
    protected void onPause() {
        super.onPause();

        Log.d(TAG, "onPause");

        if (camera != null)
            camera.release();
        camera = null;
    }

    class HolderCallback implements SurfaceHolder.Callback {

        @Override
        public void surfaceCreated(SurfaceHolder holder) {
            try {
                camera.setPreviewDisplay(holder);

                camera.setPreviewCallback(new Camera.PreviewCallback() {
                    @Override
                    public void onPreviewFrame(byte[] data, Camera camera) {
                        mLastFrame = data;
                    }
                });

                setCameraPreviewSize();

                camera.startPreview();

                Log.d(TAG, "onSurfaceCreated");
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        @Override
        public void surfaceChanged(SurfaceHolder holder, int format, int width,
                                   int height) {
            camera.stopPreview();
            setCameraDisplayOrientation(CAMERA_ID);

            Log.d(TAG, "onSurfaceChanged");

            try {
                camera.setPreviewDisplay(holder);

                camera.setPreviewCallback(new Camera.PreviewCallback() {
                    @Override
                    public void onPreviewFrame(byte[] data, Camera camera) {
                        mLastFrame = data;
                    }
                });

                setCameraPreviewSize();

                camera.startPreview();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        public void surfaceDestroyed(SurfaceHolder holder) {

        }

    }

    void setPreviewSize(boolean fullScreen) {

        // получаем размеры экрана
        Display display = getWindowManager().getDefaultDisplay();
        boolean widthIsMax = display.getWidth() > display.getHeight();

        // определяем размеры превью камеры
        Size size = camera.getParameters().getPreviewSize();

        RectF rectDisplay = new RectF();
        RectF rectPreview = new RectF();

        // RectF экрана, соотвествует размерам экрана
        rectDisplay.set(0, 0, display.getWidth(), display.getHeight());

        // RectF первью
        if (widthIsMax) {
            // превью в горизонтальной ориентации
            rectPreview.set(0, 0, size.width, size.height);
        } else {
            // превью в вертикальной ориентации
            rectPreview.set(0, 0, size.height, size.width);
        }

        Matrix matrix = new Matrix();
        // подготовка матрицы преобразования
        if (!fullScreen) {
            // если превью будет "втиснут" в экран (второй вариант из урока)
            matrix.setRectToRect(rectPreview, rectDisplay,
                    Matrix.ScaleToFit.START);
        } else {
            // если экран будет "втиснут" в превью (третий вариант из урока)
            matrix.setRectToRect(rectDisplay, rectPreview,
                    Matrix.ScaleToFit.START);
            matrix.invert(matrix);
        }
        // преобразование
        matrix.mapRect(rectPreview);

        // установка размеров surface из получившегося преобразования
        sv.getLayoutParams().height = (int) (rectPreview.bottom);
        sv.getLayoutParams().width = (int) (rectPreview.right);
    }

    void setCameraDisplayOrientation(int cameraId) {
        // определяем насколько повернут экран от нормального положения
        int rotation = getWindowManager().getDefaultDisplay().getRotation();
        int degrees = 0;
        switch (rotation) {
            case Surface.ROTATION_0:
                degrees = 0;
                break;
            case Surface.ROTATION_90:
                degrees = 90;
                break;
            case Surface.ROTATION_180:
                degrees = 180;
                break;
            case Surface.ROTATION_270:
                degrees = 270;
                break;
        }

        int result = 0;

        // получаем инфо по камере cameraId
        CameraInfo info = new CameraInfo();
        Camera.getCameraInfo(cameraId, info);

        // задняя камера
        if (info.facing == CameraInfo.CAMERA_FACING_BACK) {
            result = ((360 - degrees) + info.orientation);
        } else
            // передняя камера
            if (info.facing == CameraInfo.CAMERA_FACING_FRONT) {
                result = ((360 - degrees) - info.orientation);
                result += 360;
            }
        result = result % 360;
        camera.setDisplayOrientation(result);
    }
}
